from django.conf.urls import patterns
from django.conf.urls import url

urlpatterns = patterns('siscobros.apps.main.views',
                       url(r'^index/', 'main_view', name="vista_principal"),
                       url(r'^ciudadela/$','CiudadelaView'),
                       url(r'^ciudadela/read/$', 'serializaCiudadela', name="ConsumeCiudadela"),
                       url(r'^ciudadela/add/$','InsertCiudadela',name='RegistraCiudadela'),
                       url(r'^ciudadela/edit/(\d+)$','UpdateCiudadela'),
                       url(r'^ciudadela/del/(\d+)$','EliminaCiudadela'),
                       url(r'^costovario/$','TransCostoVario', name='TransCostoVario'),
                       url(r'^costovario/cbo/$','ComboTipoAjax',name="ComboTipo"),
                       url(r'^costovario/read/$','serializaTrans', name='ConsumeTrans'),
                       url(r'^aseguradora/$','AseguradoraView'),
                       url(r'^aseguradora/read/$','serializaAseguradora'),
                       url(r'^aseguradora/add/$','InsertAseguradora'),
                       url(r'^aseguradora/edit/(\d+)$','UpdateAseguradora'),
                       url(r'^aseguradora/del/(\d+)$','EliminaAseguradora'),
                       url(r'^reportes/costovario/$','rptcostoview'),
                       url(r'^reportes/costovario/file/$','reportecosto'),
                       )