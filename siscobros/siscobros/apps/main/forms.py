from django.forms import ModelForm
from .models import *

class T_CostoVarioForm(ModelForm):
    class Meta:
        model = Trans_Costo_Vario
        exclude = ['Estado','Fecha','Usuario']