#!/usr/bin/python
# -*- coding: utf-8 -*-
from django.shortcuts import render_to_response
from django.template import RequestContext
from datetime import datetime
from django.http import HttpResponseRedirect
# from django.http import HttpResponse
from .models import Ciudadela, Cliente, Tipo_Costo_Vario, Trans_Costo_Vario, Aseguradora
from .forms import T_CostoVarioForm
from django.core import serializers
import json
from django.contrib.auth.decorators import login_required
from django.views.decorators.csrf import csrf_exempt
from decimal import Decimal as d
from reportlab.pdfgen import canvas
from reportlab.lib.pagesizes import A4
from django.http import HttpResponse
# Create your views here.

def index_view(request):
    return render_to_response('login.html', context_instance=RequestContext(request))

@login_required(login_url="/")
def main_view(request):#prototipo de formulario de transacción
    usuario = request.user
    return render_to_response('index.html', {'usuario': usuario}, content_type=RequestContext(request))
#Crud Ciudadela-----------------------------------------------------------------------------------------------
@login_required(login_url="/")
def CiudadelaView(request):
    usuario = request.user
    return render_to_response('main/mantenimientos/Ciudadela.html', {'usuario': usuario}, context_instance=RequestContext(request))

@login_required(login_url="/")
def serializaCiudadela(request):
    data = Ciudadela.objects.all()
    resultado = []
    for Datos in data:
        serializado = {}
        serializado ['id'] = str(Datos.pk)
        serializado ['Descripcion'] = Datos.Descripcion
        resultado.append(serializado)
    Json = json.dumps(resultado)
    return HttpResponse(Json, mimetype="application/json")

@csrf_exempt
@login_required(login_url="/")
def InsertCiudadela(request):
    if request.method == 'POST':
        descripcion = request.POST['Descripcion']
        print descripcion
        ciu = Ciudadela(Descripcion=descripcion, Usuario=request.user, Fecha=datetime.today())
        print ciu
        ciu.save()
        return HttpResponse('Success', mimetype="application/json")
    else:
        return HttpResponse('Error')
@csrf_exempt
@login_required(login_url="/")
def UpdateCiudadela(request, id):
    if request.method == "POST":
        id = id
        desc = request.POST['Descripcion']
        ciudadela = Ciudadela.objects.get(id=id)
        ciudadela.Descripcion = desc
        ciudadela.Usuario = request.user
        ciudadela.Fecha = datetime.today()
        ciudadela.save()
        return HttpResponse('Success')
    else:
        return HttpResponse('Fail')

@login_required(login_url="/")
def EliminaCiudadela(request,id):
    if request.POST:
        id = id
        ciudadela = Ciudadela.objects.get(id= id)
        print ciudadela
        ciudadela.delete()
        return HttpResponse('Success')
    else:
        return HttpResponse('Fail')

@csrf_exempt
@login_required(login_url="/")
def serializaTrans(request):
    cliente = Cliente.objects.all()
    resultado = []
    for index in cliente:
        serializado = {}
        serializado ['id'] = str(index.transactions.last().id)
        serializado ['Contribuyente'] = "%s %s"%(index.Nombre, index.Apellido)
        serializado ['Fecha'] = str(index.transactions.last().Fecha)
        serializado ['Valor'] = str(index.transactions.last().Valor)
        resultado.append(serializado)
    Json = json.dumps(resultado)
    return HttpResponse(Json, mimetype="application/json")

@csrf_exempt
@login_required(login_url="/")
def ComboTipoAjax(request):
    if request.is_ajax:
        id = request.POST['id']
        valor = request.POST['titulo']
        print(id, type(valor))
        Tipocosto = Tipo_Costo_Vario.objects.get(id=id)
        resultado = Tipocosto.Valor + d(valor)
        datos = []
        print id , resultado
        serializado = {}
        serializado ['por'] = str(Tipocosto.Valor)
        serializado ['cantidad'] = str(resultado)
        datos.append(serializado)
        data = json.dumps(datos)
        return HttpResponse(data, mimetype="application/json")

@csrf_exempt
@login_required(login_url="/")
def TransCostoVario(request):
    usuario = request.user
    if request.method == 'POST':
        form = T_CostoVarioForm(request.POST)
        if form.is_valid():
            f = form.save(commit=False)
            f.Fecha = datetime.today()
            f.Usuario = usuario
            f.save()
            return HttpResponseRedirect("/costovario/")
    else:
        form = T_CostoVarioForm()
    return render_to_response('main/transacciones/TranCostoVario.html', {'usuario': usuario, 'form': form}, content_type=RequestContext(request))

@login_required(login_url="/")
def AseguradoraView(request):
    usuario = request.user
    return render_to_response('main/mantenimientos/Aseguradora.html',{'usuario':usuario},content_type=RequestContext(request))

@login_required(login_url="/")
def serializaAseguradora(request):
    data = Aseguradora.objects.all()
    resultado = []
    for Datos in data:
        serializado = {}
        serializado ['id'] = str(Datos.pk)
        serializado ['Descripcion'] = Datos.Descripcion
        resultado.append(serializado)
    Json = json.dumps(resultado)
    return HttpResponse(Json, mimetype="application/json")

@csrf_exempt
@login_required(login_url="/")
def InsertAseguradora(request):
    if request.method == "POST":
        descripcion = request.POST['Descripcion']
        aseguradora= Aseguradora(Descripcion=descripcion, Usuario=request.user, Fecha=datetime.today())
        aseguradora.save()
        return HttpResponse('Success')
    else:
        return HttpResponse('Error')

@csrf_exempt
@login_required(login_url="/")
def UpdateAseguradora(request, id):
    if request.method == "POST":
        id = id
        desc = request.POST['Descripcion']
        aseguradora = Aseguradora.objects.get(id=id)
        aseguradora.Descripcion = desc
        aseguradora.Fecha = datetime.today()
        aseguradora.Usuario = request.user
        aseguradora.save()
        return HttpResponse("Success")
    else:
        return HttpResponse("Fail")

@login_required(login_url="/")
def EliminaAseguradora(request, id):
    if request.POST:
        id = id
        aseguradora = Aseguradora.objects.get(id= id)
        aseguradora.delete()
        return HttpResponse('Success')
    else:
        return HttpResponse('Fail')

#reportes
@login_required(login_url="/")
@csrf_exempt
def rptcostoview(request):
    usuario = request.user
    return render_to_response('main/reportes/repcostovario.html',{'usuario': usuario}, content_type=RequestContext(request))

@login_required(login_url="/")
def reportecosto(request):
    if request.is_ajax:
        id = request.GET['id']
        contribuyente = request.GET['Contribuyente']
        Tipocosto = Trans_Costo_Vario.objects.get(id=id)
        valor = d(Tipocosto.Valor)
        titulo = d(Tipocosto.Titulo)
        resultado = titulo+valor
        print resultado
        print id, contribuyente
        name="costovario%s"%datetime.today()+".pdf"
        response = HttpResponse(content_type='application/pdf')
        response['Content-Disposition'] = 'attachment; filename="%s"'%(name)
        p = canvas.Canvas(response,pagesize=A4, pageCompression = 0)
        p.setLineWidth(.3)
        p.setFont('Times-Roman', 13)
        p.drawString(30,750,'REPUBLICA DEL ECUADOR')
        p.setFont('Times-Roman', 14)
        p.drawString(300,750,'VARIOS')
        p.setFont('Times-Roman', 16)
        p.drawString(30,735,'CUERPO DE BOMBEROS DE EL TRIUNFO')
        p.setFont('Times-Bold', 12)
        p.drawString(365,735,'No. ')
        p.setFont('Times-Roman', 12)
        p.drawString(385,735,'%s'%id)
        p.setFont('Times-Bold', 12)
        p.drawString(455,735,'Titulo: $')
        p.setFont('Times-Roman', 12)
        p.drawString(505,735,'%s'%titulo)#parametro
        p.setFont('Times-Bold', 12)
        p.drawString(60,725,'RUC: ')
        p.setFont('Times-Roman', 12)
        p.drawString(92,725,'0928178474001')#parametro
        p.setFont('Times-Bold', 12)
        p.drawString(455,725,'Por    : $')
        p.setFont('Times-Roman', 12)
        p.drawString(505,725,'%s'%valor)#parametro
        p.line(30,722,568,722)
        p.setFont('Times-Bold', 12)
        p.drawString(35,708,'Contribuyente:')
        p.setFont('Times-Roman', 12)
        p.drawString(118,708,'%s'%contribuyente)#parametro
        p.setFont('Times-Bold', 12)
        p.drawString(455,708,'Total : $')
        p.setFont('Times-Roman', 12)
        p.drawString(505,708,'%s'%resultado)#parametro
        p.setFont('Times-Bold', 12)
        p.drawString(35,694,'Cantidad     : $')
        p.setFont('Times-Roman', 12)
        p.drawString(117,694,'%s '%resultado)#parametro
        p.setFont('Times-Bold', 12)
        p.drawString(35,680,'Concepto     :')
        p.setFont('Times-Roman', 12)
        p.drawString(118,680,'%s'%Tipocosto.Tipo_Costo)#parametro
        p.line(30,630,568,630)
        p.setFont('Times-Bold', 12)
        p.drawString(35,610,'Fecha Emision:')
        p.setFont('Times-Roman', 12)
        p.drawString(118,610,'%s'%Tipocosto.Fecha)
        p.setFont('Times-Bold', 12)
        p.drawString(360,610,'Fecha:')
        p.setFont('Times-Roman', 12)
        p.drawString(400,610,'%s'%Tipocosto.Fecha)
        p.line(60,570,180,570)
        p.setFont('Times-Bold', 12)
        p.drawString(75,560,'%s'%contribuyente)
        p.line(380,570,500,570)
        p.setFont('Times-Bold', 12)
        p.drawString(415,560,'Tesorera')
        p.showPage()
        p.save()
        return response